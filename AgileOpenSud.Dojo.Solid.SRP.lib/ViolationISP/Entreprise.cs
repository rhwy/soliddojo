﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AgileOpenSud.Dojo.Solid.SRP.lib.ViolationISP
{
    public class Entreprise : ISimuleUneJournee
    {
        IList<ITravailleur> employés;

        public Entreprise()
        {
            employés = new List<ITravailleur>();
        }

        public void Embaucher(ITravailleur e)
        {
            employés.Add(e);
        }

        public void Licencier (ITravailleur e)
        {
            employés.Remove(e);
        }

        public void UneJourneeSePasse()
        {
            foreach (var employé in employés)
            {
                employé.Travailler();
                employé.Manger();
                employé.Dormir();
            }
        }
    }
}
