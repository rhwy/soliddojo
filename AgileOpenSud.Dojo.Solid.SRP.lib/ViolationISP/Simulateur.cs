﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AgileOpenSud.Dojo.Solid.SRP.lib.ViolationISP
{
    public class Simulateur : ISimuleUneJournee
    {
        public IList<ISimuleUneJournee> Monde;

        public Simulateur()
        {
            Monde = new List<ISimuleUneJournee>();
            
        }


        public void UneJourneeSePasse()
        {
            foreach (var item in Monde)
            {
                item.UneJourneeSePasse();
            }
        }
    }
}
