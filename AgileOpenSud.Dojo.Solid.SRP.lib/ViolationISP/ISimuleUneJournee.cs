﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AgileOpenSud.Dojo.Solid.SRP.lib.ViolationISP
{
    public interface ISimuleUneJournee
    {
        void UneJourneeSePasse();
    }
}
