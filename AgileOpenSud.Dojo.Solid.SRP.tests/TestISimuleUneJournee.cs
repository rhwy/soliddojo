﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using AgileOpenSud.Dojo.Solid.SRP.lib.ViolationISP;
using Moq;

namespace AgileOpenSud.Dojo.Solid.SRP.tests
{
    // objectif:  montrer l'interet de SRP/ISP
    // on cherche la resistance au changhement

    // voir G.R.A.S.Patterns 
    // rappeler ces fondamentaux

    // commencer avec l'interface ISimuleUneJournee
    // et faire coder la classe Entreprise qui l'implémente
    // (Simulateur est deja en place)

    // commencer par l'implementation en "dur" de Homme et Femme
    // montrer que ca demande bcp de code, et surtout que les tests ne sont pas isolés (pas de SRP sur les tests)
    // donc mettre des mocks

    // ensuite on introduit Robot et on voit le pb d'abord sur ITravailleur (ISP) 
    // et sur Entreprise (SRP)

    [TestFixture]
    public class TestISimuleUneJournee
    {      

        [Test]
        public void ScenarioFaitVivreUnHommeEtUneFemme()
        {
            //arrange
            var sim = new Simulateur();
            var etp = new Entreprise();
            sim.Monde.Add(etp);

            var mHomme = new Mock<ITravailleur>(MockBehavior.Strict);
            mHomme.Setup(h => h.Travailler()).Returns(true);
            mHomme.Setup(h => h.Manger());
            mHomme.Setup(h => h.Dormir());

            var mFemme = new Mock<ITravailleur>(MockBehavior.Strict);
            mFemme.Setup(h => h.Travailler()).Returns(true);
            mFemme.Setup(h => h.Manger());
            mFemme.Setup(h => h.Dormir());

            //act
            etp.Embaucher(mHomme.Object);
            etp.Embaucher(mFemme.Object);
            
            sim.UneJourneeSePasse();


            //assert
            mHomme.VerifyAll();
            mFemme.VerifyAll();

        }

        [Test]
        [ExpectedException]
        public void ScenarioFaitTravaillerUnHommeQuiNeMangeNiNeDors()
        {
            //arrange
            var sim = new Simulateur();
            var etp = new Entreprise();
            sim.Monde.Add(etp);

            var mHomme = new Mock<ITravailleur>(MockBehavior.Strict);

            mHomme.Setup(h => h.Travailler());

            //act
            etp.Embaucher(mHomme.Object);
            
            sim.UneJourneeSePasse();
            //assert
            mHomme.VerifyAll();

        }

        [Test]
        public void ScenarioFaitVivreUnHommeEtUneFemmeEtUnRobot()
        {
            //arrange
            var sim = new Simulateur();
            var etp = new Entreprise();
            sim.Monde.Add(etp);

            var mHomme = new Mock<ITravailleur>(MockBehavior.Strict);
            mHomme.Setup(h => h.Travailler()).Returns(true);
            mHomme.Setup(h => h.Manger());
            mHomme.Setup(h => h.Dormir());

            var mFemme = new Mock<ITravailleur>(MockBehavior.Strict);
            mFemme.Setup(h => h.Travailler()).Returns(true);
            mFemme.Setup(h => h.Manger());
            mFemme.Setup(h => h.Dormir());

            //il ne fait rien d'autre!

            //act
            etp.Embaucher(mHomme.Object);
            etp.Embaucher(mFemme.Object);
           
            // >>> si on veut embaucher un robot ??? le robot ne dort pas et ne mange pas! <<<
            // etp.Embaucher(mRobot.Object);
            
            sim.UneJourneeSePasse();


        }  //comment faire passer ce test au vert (et ceux du dessus restent vert bien sur)?

    }
}
