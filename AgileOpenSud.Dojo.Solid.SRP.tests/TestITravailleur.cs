﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using AgileOpenSud.Dojo.Solid.SRP.lib.ViolationISP;
using Moq;

namespace AgileOpenSud.Dojo.Solid.SRP.tests
{
    [TestFixture]
    public class TestITravailleur
    {
        public void TestCommentUnHommeTravaille()
        {
            ITravailleur h = new Homme();
            var leTravailEstIlFait = h.Travailler();

            Assert.That(leTravailEstIlFait, Is.True);
        }
    }
}
